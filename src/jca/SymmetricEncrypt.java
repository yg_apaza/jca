package jca;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.security.Key;

import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

public class SymmetricEncrypt
{
    static KeyGenerator keyGen;

    public static SecretKey getSecret()
    {
        try
        {
            keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
        }
        catch (Exception exp)
        {
            System.out.println("Exception inside constructor " + exp);
            exp.printStackTrace();
        }

        SecretKey secretKey = keyGen.generateKey();
        return secretKey;
    }

    public byte[] encryptData(byte[] byteDataToEncrypt, Key secretKey, String algorithm)
    {
        byte[] byteCipherText = new byte[200];
        try
        {
            Cipher aesCipher = Cipher.getInstance(algorithm);
            if (algorithm.equals("AES"))
                aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, aesCipher.getParameters());
            else if (algorithm.equals("RSA/ECB/PKCS1Padding"))
                aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byteCipherText = aesCipher.doFinal(byteDataToEncrypt);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            System.out.println("No Such Algorithm exists " + noSuchAlgo);
            noSuchAlgo.printStackTrace();
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            System.out.println("No Such Padding exists " + noSuchPad);
            noSuchPad.printStackTrace();
        }
        catch (InvalidKeyException invalidKey)
        {
            System.out.println("Invalid Key " + invalidKey);
            invalidKey.printStackTrace();
        }
        catch (BadPaddingException badPadding)
        {
            System.out.println("Bad Padding " + badPadding);
            badPadding.printStackTrace();
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            System.out.println("Illegal Block Size " + illegalBlockSize);
            illegalBlockSize.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException invalidParam)
        {
            System.out.println("Invalid Parameter " + invalidParam);
            invalidParam.printStackTrace();
        }

        return byteCipherText;
    }

    public byte[] decryptData(byte[] byteCipherText, Key secretKey, String Algorithm)
    {
        byte[] byteDecryptedText = new byte[200];
        try
        {
            Cipher aesCipher = Cipher.getInstance(Algorithm);
            if (Algorithm.equals("AES"))
                aesCipher.init(Cipher.DECRYPT_MODE, secretKey, aesCipher.getParameters());
            else if (Algorithm.equals("RSA/ECB/PKCS1Padding"))
                aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
            byteDecryptedText = aesCipher.doFinal(byteCipherText);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            System.out.println("No Such Algorithm exists " + noSuchAlgo);
            noSuchAlgo.printStackTrace();
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            System.out.println("No Such Padding exists " + noSuchPad);
            noSuchPad.printStackTrace();
        }
        catch (InvalidKeyException invalidKey)
        {
            System.out.println("Invalid Key " + invalidKey);
            invalidKey.printStackTrace();
        }
        catch (BadPaddingException badPadding)
        {
            System.out.println("Bad Padding " + badPadding);
            badPadding.printStackTrace();
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            System.out.println("Illegal Block Size " + illegalBlockSize);
            illegalBlockSize.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException invalidParam)
        {
            System.out.println("Invalid Parameter " + invalidParam);
            invalidParam.printStackTrace();
        }

        return byteDecryptedText;
    }
}