package jca;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 * 1. Encrypt data using a symmetric key
 * 2. Encrypt the symmetric key with the public key Receivers
 * 3. Create a message digest of the data to be transmitted
 * 4. Sign the message to be transmitted
 * 5. Send the data through a nonsecure channel
 * 6. Validate signature
 * 7. Decoding the message using the private key Pets for the symmetric key
 * 8. Deciphering the data using the symmetric key
 * 9. Calculate MessageDigest signed message data
 * 10.Valide if the text message digest matches the decrypted message digest of the original message
 */
public class PublicKeyCryptography
{
    public static void main(String[] args)
    {
        SymmetricEncrypt encryptUtil = new SymmetricEncrypt();
        String strDataToEncrypt = "Hello World";
        byte[] byteDataToTransmit = strDataToEncrypt.getBytes();

        // Generación de un SecretKey para el cifrado simétrico
        SecretKey senderSecretKey = SymmetricEncrypt.getSecret();

        //1. Cifrar los datos utilizando una clave simétrica
        byte[] byteCipherText = encryptUtil.encryptData(byteDataToTransmit, senderSecretKey, "AES");
        String strCipherText = DatatypeConverter.printBase64Binary(byteCipherText);

        try
        {
            // Especifique el almacén de claves que se haya importado el certificado Receptores
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] password = "testpwd".toCharArray();
            FileInputStream fis = new FileInputStream("testkeystore.ks");
            ks.load(fis, password);
            fis.close();
            
            // 2. Cifrar la clave simétrica con la clave pública 
            
            // 2.2 Creación de un certificado X509 del receptor
            X509Certificate recvcert = (X509Certificate) ks.getCertificate("testrecv");
            // 2.3 Obtención de la llave pública de los certificados
            PublicKey pubKeyReceiver = recvcert.getPublicKey();
            // 2.4 Cifrado de la SecretKey con la clave pública Receptores
            byte[] byteEncryptWithPublicKey = encryptUtil.encryptData(senderSecretKey.getEncoded(), pubKeyReceiver, "RSA/ECB/PKCS1Padding");
            String strSenbyteEncryptWithPublicKey = DatatypeConverter.printBase64Binary(byteEncryptWithPublicKey);

            // 3. Crear un resumen del mensaje de los datos a transmitir
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(byteDataToTransmit);
            byte byteMDofDataToTransmit[] = md.digest();
            String strMDofDataToTransmit = new String();
            for (int i = 0; i < byteMDofDataToTransmit.length; i++)
                strMDofDataToTransmit = strMDofDataToTransmit + Integer.toHexString((byteMDofDataToTransmit[i] & 0xFF) | 0x100).substring(1,3);


            // 3.1 Mensaje que se Firmado = clave secreta cifrada + MAC de los datos a transmitir
            String strMsgToSign = strSenbyteEncryptWithPublicKey + "|" + strMDofDataToTransmit;

            // 4. Firma el mensaje
            // 4.1 Obtener la clave privada del remitente desde el almacén de claves, proporcionando la contraseña establecida para la llave privada mientras se crea las llaves usados.
            char[] keypassword = "send123".toCharArray();
            Key myKey = ks.getKey("testsender", keypassword);
            PrivateKey myPrivateKey = (PrivateKey) myKey;

            // 4.2 Firmar el mensaje
            Signature mySign = Signature.getInstance("MD5withRSA");
            mySign.initSign(myPrivateKey);
            mySign.update(strMsgToSign.getBytes());
            byte[] byteSignedData = mySign.sign();

            // 5. Los valores byteSignedData (la firma) y strMsgToSign (los datos que se firmó) se pueden enviar a través del receptor
            
            // 6. Validar la firma
            // 6.1 Extraer la clave pública de su certificado de remitentes
            X509Certificate sendercert = (X509Certificate) ks.getCertificate("testsender");
            PublicKey pubKeySender = sendercert.getPublicKey();
            // 6.2 Verificar la Firma
            Signature myVerifySign = Signature.getInstance("MD5withRSA");
            myVerifySign.initVerify(pubKeySender);
            myVerifySign.update(strMsgToSign.getBytes());

            boolean verifySign = myVerifySign.verify(byteSignedData);
            if(verifySign)
                System.out.println("Successfully validated Signature ");
            else
                System.out.println("Error in validating Signature ");

            // 7. Descifrar el mensaje usando la llave privada Pets para obtener la clave simétrica
            char[] recvpassword = "recv123".toCharArray();
            Key recvKey = ks.getKey("testrecv", recvpassword);
            PrivateKey recvPrivateKey = (PrivateKey) recvKey;

            // Analizar el MessageDigest y el valor cifrado
            int intindexofsep = strMsgToSign.indexOf("|");
            String strEncryptWithPublicKey = strMsgToSign.substring(0, intindexofsep);
            String strHashOfData = strMsgToSign.substring(intindexofsep + 1);
            // Descifrado para obtener la clave simétrica
            byte[] bytestrEncryptWithPublicKey = DatatypeConverter.parseBase64Binary(strEncryptWithPublicKey);
            byte[] byteDecryptWithPrivateKey = encryptUtil.decryptData(bytestrEncryptWithPublicKey, recvPrivateKey, "RSA/ECB/PKCS1Padding");

            // 8. Descifrar los datos usando la clave simétrica
            SecretKeySpec secretKeySpecDecrypted = new SecretKeySpec(byteDecryptWithPrivateKey, "AES");
            byte[] byteStrCypherText = DatatypeConverter.parseBase64Binary(strCipherText);
            byte[] byteDecryptText = encryptUtil.decryptData(byteStrCypherText, secretKeySpecDecrypted, "AES");
            String strDecryptedText = new String(byteDecryptText);
            System.out.println("Decrypted data is " + strDecryptedText);

            // 9. Calcule MessageDigest de datos + mensaje firmado
            MessageDigest recvmd = MessageDigest.getInstance("MD5");
            recvmd.update(byteDecryptText);
            byte byteHashOfRecvSignedData[] = recvmd.digest();
            String strHashOfRecvSignedData = new String();
            
            for (int i = 0; i < byteHashOfRecvSignedData.length; i++)
                strHashOfRecvSignedData = strHashOfRecvSignedData + Integer.toHexString((byteMDofDataToTransmit[i] & 0xFF) | 0x100).substring(1,3);
            
            // 10. Validar si la síntesis del mensaje del texto coincide con el mensaje descifrado Digest of the Original Message
            if (!strHashOfRecvSignedData.equals(strHashOfData))
                System.out.println("Message has been tampered ");
        }
        catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | InvalidKeyException | SignatureException exp)
        {
            System.out.println(" Exception caught " + exp);
            exp.printStackTrace();
        }
    }
}
